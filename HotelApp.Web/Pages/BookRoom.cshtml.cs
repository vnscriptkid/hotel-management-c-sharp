using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HotelAppLibrary.Data;
using HotelAppLibrary.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace HotelApp.Web.Pages
{
    public class BookRoomModel : PageModel
    {
        private readonly IDatabaseData _data;
        public BookRoomModel(IDatabaseData data)
        {
            _data = data;
        }

        [BindProperty(SupportsGet = true)]
        public int RoomTypeId { get; set; }

        [BindProperty(SupportsGet = true)]
        public string FirstName { get; set; }

        [BindProperty(SupportsGet = true)]
        public string LastName { get; set; }

        [DataType(DataType.Date)]
        [BindProperty(SupportsGet = true)]
        public DateTime StartDate { get; set; }

        [DataType(DataType.Date)]
        [BindProperty(SupportsGet = true)]
        public DateTime EndDate { get; set; }

        public RoomType RoomType { get; set; }


        public void OnGet()
        {
            if (RoomTypeId > 0)
            {
                RoomType = _data.GetRoomTypeById(RoomTypeId);
            }
        }

        public IActionResult OnPost()
        {
            _data.BookGuest(FirstName, LastName, RoomTypeId, StartDate, EndDate);
            return RedirectToPage("/Index");
        }
    }
}
