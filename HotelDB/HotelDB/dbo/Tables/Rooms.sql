﻿CREATE TABLE [dbo].[Rooms]
(
	[Id] INT NOT NULL IDENTITY, 
    [RoomNumber] NVARCHAR(10) NOT NULL, 
    [RoomTypeId] INT NOT NULL, 
    PRIMARY KEY ([Id]), 
    CONSTRAINT [FK_Rooms_RoomTypes] FOREIGN KEY ([RoomTypeId]) REFERENCES [Rooms]([Id])
)
