﻿CREATE PROCEDURE [dbo].[spBookings_Search]
	@startDate date,
	@lastName nvarchar(50)
AS
begin
	select [b].[Id] as BookingId, [b].[GuestId], [b].[RoomId], [b].[StartDate], [b].[EndDate], [b].[CheckedIn], [b].[TotalCost], 
	[g].[FirstName], [g].[LastName], 
	[r].[RoomNumber], [r].[RoomTypeId], 
	[t].[Name] as RoomTypeName, [t].[Description], [t].[Price]
	from bookings b
	inner join guests g on g.Id = b.GuestId
	inner join rooms r on r.Id = b.RoomId
	inner join RoomTypes t on t.Id = r.RoomTypeId
	where g.LastName = @lastName and b.StartDate = @startDate;
end
