﻿CREATE PROCEDURE [dbo].[spBookings_CheckIn]
	@bookingId int
AS
begin
	update dbo.Bookings
	set CheckedIn = 1
	where Id = @bookingId
end
