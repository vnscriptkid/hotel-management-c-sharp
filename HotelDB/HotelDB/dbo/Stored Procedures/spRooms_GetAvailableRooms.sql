﻿CREATE PROCEDURE [dbo].[spRooms_GetAvailableRooms]
	@roomTypeId int,
	@startDate date,
	@endDate date
AS
begin
	select r.Id, r.RoomNumber, r.RoomTypeId
	from dbo.Rooms r 
	inner join dbo.RoomTypes t on r.RoomTypeId = t.Id
	where t.id = @roomTypeId	
	and r.Id not in (
		select RoomId from dbo.Bookings
		where (@startDate < EndDate and @startDate > StartDate)
		or (@endDate > StartDate and @endDate < EndDate)
		or (@startDate <= StartDate and @endDate >= EndDate)
	)
	group by r.Id, r.RoomNumber, r.RoomTypeId;
end


