﻿CREATE PROCEDURE [dbo].[spBookings_Create]
	@guestId int,
	@roomId int,
	@startDate date,
	@endDate date,
	@totalCost money
AS
begin
	set nocount on;
	insert into dbo.Bookings (GuestId, RoomId, StartDate, EndDate, TotalCost)
	values (@guestId, @roomId, @startDate, @endDate, @totalCost);
end
