﻿CREATE PROCEDURE [dbo].[spRoomTypes_GetAvailableTypes]
	@startDate date,
	@endDate date
AS
begin
	set nocount on;

	select t.Id, t.Name, t.Description, t.Price
	from dbo.Rooms r 
	inner join dbo.RoomTypes t on r.RoomTypeId = t.Id
	where r.Id not in (
		select RoomId from dbo.Bookings
		where (@startDate < EndDate and @startDate > StartDate)
		or (@endDate > StartDate and @endDate < EndDate)
		or (@startDate <= StartDate and @endDate >= EndDate)
	)
	group by t.Id, t.Name, t.Description, t.Price;
end
