﻿if not exists (select 1 from dbo.RoomTypes)
begin
    insert into dbo.RoomTypes (Name, Description, Price) 
    values ('Room Type 1', 'Description of Room Type 1', 100),    
    ('Room Type 2', 'Description of Room Type 2', 250),
    ('Room Type 3', 'Description of Room Type 3', 125);
end

if not exists (select 1 from dbo.Rooms)
begin
    declare @roomType1 int;
    declare @roomType2 int;
    declare @roomType3 int;

    select @roomType1 = Id from dbo.RoomTypes where Name = 'Room Type 1';
    select @roomType2 = Id from dbo.RoomTypes where Name = 'Room Type 2';
    select @roomType3 = Id from dbo.RoomTypes where Name = 'Room Type 3';

    insert into dbo.Rooms (RoomNumber, RoomTypeId)
    values ('101', @roomType1),
    ('102', @roomType2),
    ('103', @roomType1),
    ('104', @roomType3);
end

if not exists (select 1 from dbo.Guests)
begin
    insert into dbo.Guests (FirstName, LastName)
    values ('Thanh', 'Nguyen'),
    ('Bich', 'Le'),
    ('Cheo', 'Nguyen');
end

if not exists (select 1 from dbo.Bookings)
begin
    declare @guestId1 int;
    declare @roomId1 int;

    select @guestId1 = Id from dbo.Guests where FirstName = 'Thanh';
    select @roomId1 = Id from dbo.Rooms where RoomNumber = '102';

    insert into dbo.Bookings (GuestId, RoomId, StartDate, EndDate, TotalCost)
    values (@guestId1, @roomId1, '12/23/2020', '12/26/2020', 500);
end
