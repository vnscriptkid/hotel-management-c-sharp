﻿using HotelAppLibrary.Databases;
using HotelAppLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HotelAppLibrary.Data
{
    public class SqlData : IDatabaseData
    {
        private readonly IDataAccess _dataAccess;
        private readonly string _stringConnectionName = "SqlDb";

        public SqlData(IDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }
        public List<RoomType> GetAvailableRoomTypes(DateTime startDate, DateTime endDate)
        {
            return _dataAccess.LoadData<RoomType, dynamic>(
                "dbo.spRoomTypes_GetAvailableTypes",
                new { startDate, endDate },
                _stringConnectionName,
                isStoredProcedure: true);
        }

        public void BookGuest(string firstName, string lastName, int roomTypeId, DateTime startDate, DateTime endDate)
        {
            // create or retrieve guest (if already exists)
            Guest guest = _dataAccess.LoadData<Guest, dynamic>(
                    "dbo.spGuests_CreateOrRetrieve",
                    new { firstName, lastName },
                    _stringConnectionName,
                    isStoredProcedure: true
                ).First();

            // get list of available rooms using: roomTypeId, startDate, endDate
            // get the first room in list of available rooms
            Room room = _dataAccess.LoadData<Room, dynamic>(
                    "dbo.spRooms_GetAvailableRooms",
                    new { roomTypeId, startDate, endDate },
                    _stringConnectionName,
                    isStoredProcedure: true
                ).First();

            // get price of room
            RoomType roomType = _dataAccess.LoadData<RoomType, dynamic>(
                "select * from dbo.RoomTypes where Id=@Id",
                new { Id = room.RoomTypeId },
                _stringConnectionName).First();

            TimeSpan timeStaying = endDate.Date.Subtract(startDate.Date);
            decimal totalCost = roomType.Price * timeStaying.Days;

            // persist a booking record
            _dataAccess.SaveData(
                "dbo.spBookings_Create",
                new
                {
                    guestId = guest.Id,
                    roomId = room.Id,
                    totalCost,
                    startDate,
                    endDate
                },
                _stringConnectionName,
                isStoredProcedure: true);
        }

        public List<BookingFull> SearchForBookings(string lastName)
        {
            return _dataAccess.LoadData<BookingFull, dynamic>("dbo.spBookings_Search",
                                                          new { startDate = DateTime.Now.Date, lastName },
                                                          _stringConnectionName,
                                                          isStoredProcedure: true);
        }

        public void CheckInGuest(int bookingId)
        {
            _dataAccess.SaveData("dbo.spBookings_CheckIn", new { bookingId }, _stringConnectionName, isStoredProcedure: true);
        }

        public RoomType GetRoomTypeById(int id)
        {
            return _dataAccess.LoadData<RoomType, dynamic>("dbo.spRoomTypes_GetById",
                                        new { id },
                                        _stringConnectionName,
                                        isStoredProcedure: true).FirstOrDefault();
        }
    }
}
