﻿using System.Collections.Generic;

namespace HotelAppLibrary.Databases
{
    public interface IDataAccess
    {
        List<T> LoadData<T, U>(string sqlStatement, U parameters, string connectionStringName, bool isStoredProcedure = false);
        void SaveData<U>(string sqlStatement, U parameters, string connectionStringName, bool isStoredProcedure = false);
    }
}