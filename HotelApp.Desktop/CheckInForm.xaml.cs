﻿using HotelAppLibrary.Data;
using HotelAppLibrary.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HotelApp.Desktop
{
    /// <summary>
    /// Interaction logic for CheckInForm.xaml
    /// </summary>
    public partial class CheckInForm : Window
    {
        private readonly IDatabaseData _data;
        private BookingFull _bookingData;
        private int BookingId { get; set; }

        public CheckInForm(IDatabaseData data)
        {
            InitializeComponent();
            _data = data;
        }

        public void InitBookingData(BookingFull bookingData)
        {
            _bookingData = bookingData;
            fullNameText.Text = $"{bookingData.FirstName} {bookingData.LastName}";
            roomTypeText.Text = bookingData.RoomTypeName;
            roomNumberText.Text = bookingData.RoomNumber;
            BookingId = bookingData.BookingId;
            totalCostText.Text = string.Format("{0:C}", bookingData.TotalCost);
        }

        private void checkInButton_Click(object sender, RoutedEventArgs e)
        {
            _data.CheckInGuest(BookingId);
            this.Close();
        }
    }
}
