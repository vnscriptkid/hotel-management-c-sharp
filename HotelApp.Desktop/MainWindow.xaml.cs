﻿using HotelAppLibrary.Data;
using HotelAppLibrary.Databases;
using HotelAppLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Extensions.DependencyInjection;

namespace HotelApp.Desktop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly IDatabaseData _data;

        public MainWindow(IDatabaseData data)
        {
            InitializeComponent();
            _data = data;
            //resultsList.ItemsSource = new List<dynamic>
            //{
            //    new { FirstName = "Thanh", LastName = "Nguyen", RoomTypeName = "King", RoomNumber = "123", TotalCost = 120.57M },
            //    new { FirstName = "Thanh", LastName = "Nguyen", RoomTypeName = "King", RoomNumber = "123", TotalCost = 120.57M },
            //    new { FirstName = "Thanh", LastName = "Nguyen", RoomTypeName = "King", RoomNumber = "123", TotalCost = 120.57M },
            //    new { FirstName = "Thanh", LastName = "Nguyen", RoomTypeName = "King", RoomNumber = "123", TotalCost = 120.57M }
            //};
        }

        private void searchBookingsButton_Click(object sender, RoutedEventArgs e)
        {
            List<BookingFull> bookings = _data.SearchForBookings(lastNameText.Text);
            resultsList.ItemsSource = bookings;
        }

        private void CheckInButton_Click(object sender, RoutedEventArgs e)
        {
            var checkInWindow = App.serviceProvider.GetService<CheckInForm>();
            var bookingData = (BookingFull) ((Button) e.Source).DataContext;
            checkInWindow.InitBookingData(bookingData);
            checkInWindow.Show();
        }
    }
}
